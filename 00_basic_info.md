# creación de ambientes virtuales

`pip install virtualenv`

Si se requieren permisos administrativos
`sudo pip install virtualenv`

para configurar diferentes ambientes virtuales
```
pip install virtualenvwrapper
vi .bashrc
```

```
export WORKON_HOME=~/virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3

source /usr/local/bin/virtualenvwrapper.sh
```

```
cd ~
cd virtualenvs
mkvirtualenv --python=/usr/local/bin/python3 jupyter
```

instalar componentes
```
pip install --upgrade pip
pip install numpy pandas matplotlib
pip install jupyter jupyterlab
jupyter lab
```

si se instala wirtualenv wrapper
`workon jupyter`

sin virtualenv
`source ~/virtualenvs/jupyter/bin/activate`

para desactivar el virtualenv
`deactivate`

crear pythonpath
`export PYTHONPATH="/Users/matt/my_project:$PYTHONPATH"`

si se tiene virtualenv wrapper
```
export OLD_PYTHONPATH="$PYTHONPATH"
export PYTHONPATH="/Users/matt/my_project:$PYTHONPATH"
export PYTHONPATH="$OLD_PYTHONPATH"
```

source: [medium](https://medium.com/@MattGosden/set-up-your-mac-for-python-and-jupyter-using-virtual-environments-730bf2888e05)
