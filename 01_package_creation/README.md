**Sample Package Creation**
---
Este se ejecuta de la siguiente forma

```
python setup.py bdistwheel
```

luego para instalar el binario se hace de la siguiente forma

```
python -m pip install analytics_pkg-0.1-py2-none-any.whl
```

Para ejecutarlo en jupyter notebook

```
import analytics_pkg
analytics_pkg.business_logic()
```
