from setuptools import setup
with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='analytics_pkg',
    version='0.1',
    description='Este es un paquete analitico de ejemplo',
    url='http://gitlab.com/maripan/analytics_pkg',
    author='Rodolfo Maripan',
    author_email='my-email@falabella.cl',
    license='MIT',
    long_description=long_description,
    long_description_content_type="text/markdown",
     url="https://gitlab.com/maripan/python-tutorials",
    packages=['analytics_pkg'],
    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
    zip_safe=False
)
